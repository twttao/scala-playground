package Chapter8

import Chapter5.Stream
import Chapter6.RNG
import Chapter8.Prop.TestCases

case class Prop(run: (TestCases, RNG) => Result) {
  def &&(p: Prop) = Prop {
    (n, rng) => run(n, rng) match {   // first run left side
      case Passed => p.run(n, rng)    // only run right side if left passes
      case x => x   // otherwise report left side failure
    }
  }

  def ||(p: Prop) = Prop {
    (n, rng) => run(n, rng) match {   // first run left side
      case Falsified(msg, _) => p.tag(msg).run(n, rng)    // bring failure message to and run right side
      case x => x   // otherwise report success
    }
  }

  def tag(msg: String) = Prop {
    (n, rng) => run(n, rng) match {
      case Falsified(e, c) => Falsified(msg + "\n" + e, c)
      case x => x
    }
  }
}

object Prop {
  type TestCases = Int

  def forAll[A](as: Gen[A])(f: A => Boolean): Prop = new Prop ((n,rng) =>
    randomStream(as)(rng).zip(Stream.from(0)).take(n).map {
      case (a, i) => try {
        if (f(a)) Passed else Falsified(a.toString, i)
      } catch { case e: Exception => Falsified(buildMsg(a, e), i) }
    }.find(_.isFalsified).getOrElse(Passed)
  )

  def randomStream[A](g: Gen[A])(rng: RNG): Stream[A] =
    Stream.unfold(rng)(rng => Some(g.sample.run(rng)))

  def buildMsg[A](s: A, e: Exception): String =
    s"test case: $s\n" +
      s"generated an exception: ${e.getMessage}\n" +
      s"stack trace:\n ${e.getStackTrace.mkString("\n")}"
}
