package Chapter8

object types {
  type FailedCase = String
  type SuccessCount = Int
  type Result = Option[(FailedCase, SuccessCount)]
}
