package Chapter10

trait Monoid[A] {
  def op(a1: A, a2: A): A
  def zero: A
}

object Monoid {
  def concatenate[A](as: List[A], m: Monoid[A]): A = as.foldLeft(m.zero)(m.op)

  def endoMonoid[A]: Monoid[A => A] = new Monoid[A => A] {
    def op(f: A => A, g: A => A): A => A = f compose g

    def zero: A => A = a => a
  }

  val intAddition: Monoid[Int] = new Monoid[Int] {
    def op(a1: Int, a2: Int): Int = a1 + a2
    def zero: Int = 0
  }

  def foldMap[A,B](as: List[A], m: Monoid[B])(f: A => B): B = concatenate(as.map(f), m)

  def productMonoid[A,B](A: Monoid[A], B: Monoid[B]): Monoid[(A,B)] = new Monoid[(A, B)] {
    override def op(pair1: (A, B), pair2: (A, B)): (A, B) = (
      A.op(pair1._1, pair2._1), B.op(pair1._2, pair2._2)
    )

    override def zero: (A, B) = (A.zero, B.zero)
  }

  def functionMonoid[A,B](B: Monoid[B]): Monoid[A => B] = new Monoid[A => B] {
    def op(f1: A => B, f2: A => B): A => B = a => B.op(f1(a), f2(a))

    def zero: A => B = _ => B.zero
  }

  def mapMergeMonoid[K,V](V: Monoid[V]): Monoid[Map[K, V]] =
    new Monoid[Map[K, V]] {
      def op(m1: Map[K, V], m2: Map[K, V]): Map[K, V] =
        (m1.keySet ++ m2.keySet).foldLeft(zero)((mAcc, k) =>
          mAcc.updated(k, V.op(
            m1.getOrElse(k, V.zero),
            m2.getOrElse(k, V.zero)))
        )

      def zero: Map[K, V] = Map[K, V]()
    }
}
