package Chapter10

import Monoid._

object Exercise10_18 extends App {
  def bag[A](as: IndexedSeq[A]): Map[A, Int] = {
    val bagMapMergeMonoid: Monoid[Map[A, Int]] = mapMergeMonoid(intAddition)
    foldMap(as.toList, bagMapMergeMonoid)(a => Map(a -> 1))
  }

  val result = bag(Vector("a", "rose", "is", "a", "rose"))
  println(result)
}
