package Chapter10

import Monoid._

object Exercise10_6 {
  // foldMap by foldLeft
  def foldMap[A,B](as: List[A], m: Monoid[B])(f: A => B): B =
    as.foldLeft(m.zero)((b, a) => m.op(b, f(a)))

  // foldMap by foldRight
  def foldMapV2[A,B](as: List[A], m: Monoid[B])(f: A => B): B =
    as.foldRight(m.zero)((a, b) => m.op(f(a), b))

  // foldRight by foldMap (official; by endoMonoid)
  //  def foldRight[A, B](as: List[A])(z: B)(f: (A, B) => B): B =
  //    foldMap(as, endoMonoid[B])(f.curried)(z)
}
