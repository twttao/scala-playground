package Chapter6

import Chapter6.RNG._

object IntDouble extends App {
  println(intDouble(SimpleRNG(42)))
  println(intDouble(SimpleRNG(42)))
  println(intDouble(intDouble(SimpleRNG(42))._2))
  println(intDouble(intDouble(SimpleRNG(42))._2))
}
