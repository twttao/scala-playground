package Chapter6

object MachineTest extends App {
  val inputs = List(Coin, Turn, Coin, Turn, Coin, Turn, Coin, Turn)
  val ((candies, coins), _) = Machine.simulateMachine(inputs).run(Machine.init)
  println(candies, coins)

  val inputs2 = List(Coin, Coin, Turn, Turn)
  val ((candies2, coins2), _) = Machine.simulateMachine(inputs2).run(Machine.init)
  println(candies2, coins2)
}
