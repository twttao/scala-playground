package Chapter6

import Chapter6.RNG.ints

object Ints extends App {
  println(ints(5)(SimpleRNG(42)))
  println(ints(0)(SimpleRNG(42)))
}
