package Chapter6

import Chapter6.RNG._

object Double3 extends App {
  println(double3(SimpleRNG(42)))
  println(double3(SimpleRNG(42)))
  println(double3(double3(SimpleRNG(42))._2))
  println(double3(double3(SimpleRNG(42))._2))
}
