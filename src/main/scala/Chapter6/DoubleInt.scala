package Chapter6

import Chapter6.RNG._

object DoubleInt extends App {
  println(doubleInt(SimpleRNG(42)))
  println(doubleInt(SimpleRNG(42)))
  println(doubleInt(doubleInt(SimpleRNG(42))._2))
  println(doubleInt(doubleInt(SimpleRNG(42))._2))
}
