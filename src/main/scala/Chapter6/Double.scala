package Chapter6

import Chapter6.RNG._

object Double extends App {
  println(double(SimpleRNG(42)))
  println(doubleV2(SimpleRNG(42)))
  println(double(SimpleRNG(42)))
  println(doubleV2(SimpleRNG(42)))
  println(double(double(SimpleRNG(42))._2))
  println(doubleV2(double(SimpleRNG(42))._2))
  println(double(double(SimpleRNG(42))._2))
  println(doubleV2(double(SimpleRNG(42))._2))
}
