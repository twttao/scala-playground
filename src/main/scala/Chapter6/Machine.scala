package Chapter6

import Chapter6.State._

sealed trait Input
case object Coin extends Input
case object Turn extends Input

case class Machine(locked: Boolean, candies: Int, coins: Int)

object Machine {
  val init: Machine = Machine(locked = true, 5, 10)

  def simulateMachine(inputs: List[Input]): State[Machine, (Int, Int)] =
    sequence(inputs.map {
      case Coin => whenCoin
      case Turn => whenTurn
    }).map(results => results.last)

  private def whenCoin: State[Machine, (Int, Int)] = State(m =>
    if (m.locked && m.candies > 0) {
      (for (
        _ <- insert;
        s <- unlock
      ) yield s).run(m)
    } else {
      insert.run(m)
    }
  )

  private def whenTurn: State[Machine, (Int, Int)] = State(m =>
    if (!m.locked) {
      (for (
        _ <- dispense;
        s <- lock
      ) yield s).run(m)
    } else {
      get(m)
    }
  )

  private def insert: State[Machine, (Int, Int)] =
    State(m => get(Machine(m.locked, m.candies, m.coins + 1)))

  private def unlock: State[Machine, (Int, Int)] =
    State(m => get(Machine(locked = false, m.candies, m.coins)))

  private def lock: State[Machine, (Int, Int)] =
    State(m => get(Machine(locked = true, m.candies, m.coins)))

  private def dispense: State[Machine, (Int, Int)] =
    State(m => get(Machine(locked = true, m.candies - 1, m.coins)))

  private def get(m: Machine): ((Int, Int), Machine) =
    ((m.candies, m.coins), m)
}
