package Chapter6

import Chapter6.RNG._

object NonNegativeInt extends App {
  println(nonNegativeInt(SimpleRNG(42)))
  println(nonNegativeInt(SimpleRNG(42)))
  println(nonNegativeInt(nonNegativeInt(SimpleRNG(42))._2))
  println(nonNegativeInt(nonNegativeInt(SimpleRNG(42))._2))
}
