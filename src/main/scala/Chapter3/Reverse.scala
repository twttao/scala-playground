package Chapter3

object Reverse extends App {
  println(List.apply(1, 2, 3, 4, 5).reverse)
  println(List.apply(5, 4, 3, 2, 1).reverse)
  println(Nil.reverse)
}
