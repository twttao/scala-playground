package Chapter3

object Append extends App {
  println(List.apply(1, 2).append(List.apply(3, 4)))
  println(Nil.append(List.apply(1, 2)))
  println(List.apply(1, 2).append(Nil))
}
