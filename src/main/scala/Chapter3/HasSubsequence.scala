package Chapter3

object HasSubsequence extends App {
  val list1 = List.apply(1, 2, 3, 4, 5)

  val list2 = List.apply(2, 3, 4)
  val list3 = List.apply(4, 5)

  val list4 = List.apply(0, 1, 2)
  val list5 = List.apply(4, 5, 6)
  val list6 = List.apply(2, 4)

  println(List.hasSubsequence(list1, Nil))
  println(List.hasSubsequence(Nil, list1))
  println(List.hasSubsequence(Nil, Nil))

  println(List.hasSubsequence(list1, list2))
  println(List.hasSubsequence(list1, list3))

  println(List.hasSubsequence(list1, list4))
  println(List.hasSubsequence(list1, list5))
  println(List.hasSubsequence(list1, list6))
}
