package Chapter3

object Filter extends App {
  val list = List.apply(1, 2, 3, 4, 5, 6, 7)

  println(list filter (_ % 2 == 0))
  println(list filter2 (_ % 2 == 0))
  println((Nil: List[Int]) filter (_ % 2 == 0))
  println((Nil: List[Int]) filter2 (_ % 2 == 0))
}
