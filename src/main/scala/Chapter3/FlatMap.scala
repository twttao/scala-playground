package Chapter3

object FlatMap extends App {
  val list = List.apply(1, 2, 3)

  println(list.flatMap(i => List.apply(i, i, i)))
}
