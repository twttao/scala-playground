package Chapter3

object DropWhile extends App {
  val list = List.apply(1, 2, 3, 4, 5)

  println(list.dropWhile((x: Int) => x % 2 != 0))
  println(list.dropWhile(_ => true))
  println(list.dropWhile(_ => false))
  println(Nil.dropWhile(_ => false))
}
