package Chapter3

sealed trait Tree[+A] {
  def size: Int = this.fold(_ => 1)(_ + _ + 1)

  def depth: Int = this.fold(_ => 1)((l, r) => (l max r) + 1)

  def map[B](f: A => B): Tree[B] = this.fold(v => Leaf(f(v)): Tree[B])((l, r) => Branch(l, r))

  def fold[B](f1: A => B)(f2: (B, B) => B): B = this match {
    case Leaf(v) => f1(v)
    case Branch(l, r) => f2(l.fold(f1)(f2), r.fold(f1)(f2))
  }
}

object Tree {
  def maximum(t: Tree[Int]): Int = t match {
    case Branch(l, r) => maximum(l) max maximum(r)
    case Leaf(v) => v
  }
}

case class Leaf[A](value: A) extends Tree[A]
case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]
