package Chapter3

object PlusOne extends App {
  var list = List.apply(1, 2, 3, 4, 5)

  println(List.plusOne(list))
  println(List.plusOne(Nil))
}
