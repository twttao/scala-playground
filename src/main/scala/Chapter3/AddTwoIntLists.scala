package Chapter3

object AddTwoIntLists extends App {
  val list1 = List.apply(1, 2, 3)
  val list2 = List.apply(4, 5, 6)
  val list3 = List.apply(7, 8)

  println(List.addTwoIntLists(list1, list2))
  println(List.addTwoIntLists(list1, list3))
  println(List.addTwoIntLists(list3, list1))

  println(List.addTwoIntLists(list1, Nil))
  println(List.addTwoIntLists(Nil, list2))
  println(List.addTwoIntLists(Nil, Nil))
}
