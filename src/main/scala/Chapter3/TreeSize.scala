package Chapter3

object TreeSize extends App {
  val tree1 = Branch(Branch(Leaf(1), Leaf(2)), Leaf(3))
  val tree2 = Branch(Leaf(1), Leaf(2))

  println(tree1.size)
  println(tree2.size)
}
