package Chapter3

object TreeMaximum extends App {
  val tree1 = Branch(Branch(Leaf(1), Leaf(2)), Leaf(3))
  val tree2 = Branch(Leaf(1), Leaf(2))

  println(Tree.maximum(tree1))
  println(Tree.maximum(tree2))
}
