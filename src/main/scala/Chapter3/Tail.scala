package Chapter3

object Tail extends App {
  val list = List.apply(1, 2, 3)

  println(list.tail)
  println(Nil.tail)
}
