package Chapter3

object TreeMap extends App {
  val tree = Branch(Branch(Leaf(1), Leaf(2)), Branch(Branch(Leaf(1), Leaf(2)), Leaf(2)))

  println(tree map (_ * 2))
}
