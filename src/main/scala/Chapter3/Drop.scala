package Chapter3

object Drop extends App {
  val list = List.apply(1,2,3,4)
  println(list.drop(-1))
  println(list.drop(0))
  println(list.drop(1))
  println(list.drop(2))
  println(list.drop(3))
  println(list.drop(4))
  println(list.drop(5))
}
