package Chapter3

object Length extends App {
  println(List.apply(1, 2, 3, 4, 5).length)
  println(List.apply(1, 2, 3, 4, 5).length2)
  println(List.apply(1, 2, 3).length)
  println(List.apply(1, 2, 3).length2)
  println(Nil.length)
  println(Nil.length2)
}
