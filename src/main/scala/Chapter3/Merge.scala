package Chapter3

object Merge extends App {
  val list1 = List.apply(1, 2)
  val list2 = List.apply(3, 4)
  val list3 = List.apply(5, 6)
  println(List.merge(List.apply(list1, list2, list3)))

  println(List.merge(List.apply(Nil, list1)))
  println(List.merge(List.apply(list1, list2, Nil, list3, Nil)))
  println(List.merge(List.apply(Nil, Nil, Nil)))
}
