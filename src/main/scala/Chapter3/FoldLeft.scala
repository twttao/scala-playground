package Chapter3

object FoldLeft extends App {
  println(List.apply(1, 2, 3, 4).foldLeft(0)(_ + _))
  println(List.apply(1, 2, 3, 4).foldLeft(1)(_ * _))
}
