package Chapter3

object FoldRight extends App {
  println(List.apply(1, 2, 3).foldRight(0)(_ + _))
  println(List.apply(1, 2, 3).foldRight2(0)(_ + _))
  println(List.apply(1, 2, 3, 4).foldRight(1)(_ * _))
  println(List.apply(1, 2, 3, 4).foldRight2(1)(_ * _))
  println(List.apply(1, 2, 3, 4).foldRight(Nil: List[Int])(Cons(_, _)))
  println(List.apply(1, 2, 3, 4).foldRight2(Nil: List[Int])(Cons(_, _)))
}
