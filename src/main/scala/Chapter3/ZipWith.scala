package Chapter3

object ZipWith extends App {
  val list1 = List.apply(1, 2, 3, 4, 5)
  val list2 = List.apply(2, 3, 4, 5)

  println((list1 zipWith list2)(_ + _))
}
