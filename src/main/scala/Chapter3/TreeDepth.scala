package Chapter3

object TreeDepth extends App {
  val tree1 = Branch(Branch(Leaf(1), Leaf(2)), Leaf(3))
  val tree2 = Branch(Leaf(1), Leaf(2))
  val tree3 = Branch(Branch(Leaf(1), Leaf(2)), Branch(Branch(Leaf(1), Leaf(2)), Leaf(2)))

  println(tree1.depth)
  println(tree2.depth)
  println(tree3.depth)
}
