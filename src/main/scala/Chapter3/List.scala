package Chapter3

import scala.annotation.tailrec

sealed trait List[+A] {
  def tail: List[A] = this match {
    case Nil => Nil
    case Cons(_, xs) => xs
  }

  def head: A = this match {
    case Nil => throw new Exception("access Head of empty List")
    case Cons(x, _) => x
  }

  def setHead[S >: A](a: S): List[S] = this match {
    case Nil => Cons(a, Nil)
    case Cons(_, xs) => Cons(a, xs)
  }

  @tailrec
  final def drop(n: Int): List[A] = this match {
    case Nil => Nil
    case Cons(_, xs) =>
      if (n <= 0) {
        this
      } else {
        xs.drop(n - 1)
      }
  }

  @tailrec
  final def dropWhile (f: A => Boolean): List[A] = this match {
    case Nil => Nil
    case Cons(x, xs) =>
      if (f(x)) {
        xs.dropWhile(f)
      } else {
        this
      }
  }

  def init: List[A] = this match {
    case Nil => Nil
    case Cons(x, xs) =>
      if (xs == Nil) {
        Nil
      } else {
        Cons(x, xs.init)
      }
  }

  def foldRight[B] (z: B)(f: (A, B) => B): B = this match {
    case Nil => z
    case Cons(x, xs) => f(x, xs.foldRight(z)(f))
  }

  def foldRight2[B] (z: B)(f: (A, B) => B): B = {
    this.reverse.foldLeft(z)((b: B, a: A) => f(a, b))
  }

  @tailrec
  final def foldLeft[B] (z: B)(f: (B, A) => B): B = this match {
    case Nil => z
    case Cons(x, xs) =>
      if (xs == Nil) {
        f(z, x)
      } else {
        xs.foldLeft(f(z, x))(f)
      }
  }

  def length: Int = {
    this.foldRight(0)((_, b: Int) => b + 1)
  }

  def length2: Int = {
    this.foldLeft(0)((b: Int, _) => b + 1)
  }

  def reverse: List[A] = {
    this.foldLeft(Nil: List[A])((b: List[A], a: A) => Cons(a, b))
  }

  def append[S >: A] (l: List[S]): List[S] = {
    this.foldRight(l)((a: A, b: List[S]) => Cons(a, b))
  }

  def map[B](f: A => B): List[B] = this match {
    case Nil => Nil
    case Cons(x, xs) => Cons(f(x), xs.map(f))
  }

  def filter(f: A => Boolean): List[A] = this match {
    case Nil => Nil
    case Cons(x, xs) =>
      if(f(x)) {
        Cons(x, xs.filter(f))
      } else {
        xs.filter(f)
      }
  }

  def filter2(f: A => Boolean): List[A] = {
    this.flatMap(a => if(f(a)) List.apply(a) else Nil)
  }

  def flatMap[B](f: A => List[B]): List[B] = {
    val value = this.map(f)
    List.merge(value)
  }

  def zipWith[S >: A](as: List[S])(f: (S, S) => S): List[S] = this match {
    case Nil => as
    case Cons(x, xs) =>
      if(as == Nil) {
        this
      } else {
        Cons(f(x, as.head), xs.zipWith(as.tail)(f))
      }
  }
}

case object Nil extends List[Nothing]
case class Cons[+A] (_head: A, _tail: List[A]) extends List[A]

object List {
  def sum (ints: List[Int]): Int = ints match {
    case Nil => 0
    case Cons (x, xs) => x + sum(xs)
  }

  def product (ds: List[Double]): Double = ds match {
    case Nil => 1.0
    case Cons(0.0, _) => 0.0
    case Cons(x, xs) => x * product(xs)
  }

  def apply[A] (as: A*): List[A] =
    if (as.isEmpty) {
      Nil
    } else {
      Cons(as.head, apply(as.tail: _*))
    }

  def merge[A](ls: List[List[A]]): List[A] = {
    def appendFoldRight (l: List[A], z: List[A], rest: List[List[A]]): List[A] = l match {
      case Nil =>
        if (rest == Nil) {
          z
        }
        else {
          appendFoldRight(z, rest.head, rest.tail)
        }
      case Cons(x, xs) =>
        if (xs == Nil)
          if (rest == Nil) {
            Cons(x, z)
          }
          else {
            Cons(x, appendFoldRight(z, rest.head, rest.tail))
          }
        else {
          Cons(x, appendFoldRight(xs, z, rest))
        }
    }

    if(ls == Nil) {
      Nil
    } else {
      appendFoldRight(ls.head, ls.tail.head, ls.tail.tail)
    }
  }

  def plusOne(as: List[Int]): List[Int] = as match {
    case Nil => Nil
    case Cons(x, xs) => Cons(x + 1, plusOne(xs))
  }

  def doubleToString(as: List[Double]): List[String] = as match {
    case Nil => Nil
    case Cons(x, xs) => Cons(x.toString, doubleToString(xs))
  }

  def addTwoIntLists(ints1: List[Int], ints2: List[Int]): List[Int] = ints1 match {
    case Nil => ints2
    case Cons(x, xs) =>
      if(ints2 == Nil) {
        ints1
      } else {
        Cons(x + ints2.head, addTwoIntLists(xs, ints2.tail))
      }
  }

  @tailrec
  def hasSubsequence[A](sup: List[A], sub: List[A], fromBeginning: Boolean = false): Boolean = sup match {
    case Nil => sub == Nil
    case Cons(x, xs) =>
      if(sub == Nil) {
        true
      }
      else
        if(x == sub.head) {
          hasSubsequence(xs, sub.tail, fromBeginning = true)
        }
        else
          if(fromBeginning) {
            false
          }
          else {
            hasSubsequence(xs, sub)
          }
  }
}
