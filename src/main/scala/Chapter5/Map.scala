package Chapter5

object Map extends App {
  val stream = Stream.apply({1 + 1}, {2 + 2}, {3 + 3}, {4 + 4})

  println(Empty.map(_ => "good"))
  println(Empty.mapV2(_ => "good"))
  println(stream.map(_ / 2).toList)
  println(stream.mapV2(_ / 2).toList)
}
