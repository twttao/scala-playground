package Chapter5

import scala.annotation.tailrec

sealed trait Stream[+A] {
  def headOption: Option[A] = this match {
    case Empty => None
    case Cons(h, _) => Some(h())
  }

  def toList: List[A] = this match {
    case Empty => Nil
    case Cons(h, t) => h()::t().toList
  }

  def toListSafe: List[A] = {
    @tailrec
    def go(s: Stream[A], acc: List[A]): List[A] = s match {
      case Empty => acc.reverse
      case Cons(h, t) => go(t(), h()::acc)
    }
    go(this, Nil)
  }

  def take(n: Int): Stream[A] = this match {
    case Cons(h, t) if n > 0 => Cons(h, () => t().take(n - 1))
    case _ => Empty
  }

  @tailrec
  final def drop(n: Int): Stream[A] = this match {
    case Cons(_, t) if n > 0 =>  t().drop(n - 1)
    case _ => this
  }

  def takeWhile(p: A => Boolean): Stream[A] = this match {
    case Cons(h, t) if p(h()) => Cons(h, () => t() takeWhile p)
    case _ => Empty
  }

  @tailrec
  final def exists(p: A => Boolean): Boolean = this match {
    case Cons(h, t) => p(h()) || t().exists(p)
    case _ => false
  }

  def foldRight[B](z: => B)(f: (A, => B) => B): B = this match {
    case Cons(h, t) => f(h(), t().foldRight(z)(f))
    case _ => z
  }

  def existsV2(p: A => Boolean): Boolean =
    foldRight(false)((a, b) => p(a) || b)

  def forAll(f: A => Boolean): Boolean =
    foldRight(true)((a, b) => f(a) && b)

  def takeWhileV2(p: A => Boolean): Stream[A] =
    foldRight(Empty: Stream[A])((a, b) =>
      if(p(a)) {
        Stream.cons(a, b)
      } else {
        Empty
      }
    )

  def headOptionV2: Option[A] =
    foldRight(None: Option[A])((a, _) => Some(a))

  def map[B](f: A => B): Stream[B] =
    foldRight(Empty: Stream[B])((a, b) => Stream.cons(f(a), b))

  def filter(f: A => Boolean): Stream[A] =
    foldRight(Empty: Stream[A])((a, b) => if (f(a)) Stream.cons(a, b) else b)

  def append[B>:A](s: => Stream[B]): Stream[B] =
    foldRight(s)((a, b) => Stream.cons(a, b))

  def flatMap[B](f: A => Stream[B]): Stream[B] =
    foldRight(Empty: Stream[B])((a, b) => f(a).append(b))

  def mapV2[B](f: A => B): Stream[B] = Stream.unfold(this){
    case Cons(a, _as) => Some((f(a()), _as()))
    case Empty => None
  }

  def takeV2(n: Int): Stream[A] = Stream.unfold((this, n)){
    case (Cons(a, _as), _n) if _n > 0 => Some(a(), (_as(), _n - 1))
    case _ => None
  }

  def takeWhileV3(p: A => Boolean): Stream[A] = Stream.unfold(this){
    case Cons(a, _as) if p(a()) => Some(a(), _as())
    case _ => None
  }

  def zipWith[B,C](bs: Stream[B])(f: (A,B) => C): Stream[C] = Stream.unfold((this, bs)){
    case (Cons(a, _as), Cons(b, _bs)) => Some(f(a(), b()), (_as(), _bs()))
    case _ => None
  }

  def zip[B](s2: Stream[B]): Stream[(A,B)] =
    zipWith(s2)((_,_))

  def zipAll[B](bs: Stream[B]): Stream[(Option[A], Option[B])] = Stream.unfold((this, bs)){
    case (Cons(a, _as), Cons(b, _bs)) => Some((Some(a()), Some(b())), (_as(), _bs()))
    case (Cons(a, _as), Empty) => Some((Some(a()), None), (_as(), Empty))
    case (Empty, Cons(b, _bs)) => Some((None, Some(b())), (Empty, _bs()))
    case _ => None
  }

  @tailrec
  final def startsWith[S>:A](s: Stream[S]): Boolean = (this, s) match {
    case (Cons(a, _as), Cons(aa, _aas)) => a() == aa() && _as().startsWith(_aas())
    case (_, Empty) => true
    case _ => false
  }

  def startsWithV2[S>:A](s: Stream[S]): Boolean = this.zipAll(s).takeWhile(_._2.isDefined) forAll {
    case (h, h2) => h == h2
  }

  def tails: Stream[Stream[A]] = Stream.unfold(this){
    case Empty => None
    case s => Some((s, s drop 1))
  } append Stream.apply(Empty)

  def scanRight[B](z: B)(f: (A, => B) => B): Stream[B] =
    this.tails.map(t => t.foldRight(z)(f))

  @annotation.tailrec
  final def find(f: A => Boolean): Option[A] = this match {
    case Empty => None
    case Cons(h, t) => if (f(h())) Some(h()) else t().find(f)
  }
}

case object Empty extends Stream[Nothing]
case class Cons[+A](h: () => A, t: () => Stream[A]) extends Stream[A]

object Stream {
  def cons[A](hd: => A, tl: => Stream[A]): Stream[A] = {
    lazy val head = hd
    lazy val tail = tl
    Cons(() => head, () => tail)
  }

  def empty[A]: Stream[A] = Empty

  def apply[A](as: A*): Stream[A] =
    if (as.isEmpty) empty else cons(as.head, apply(as.tail: _*))

  def ones: Stream[Int] = cons(1, ones)

  def constant[A](a: A): Stream[A] = cons(a, constant(a))

  def from(n: Int): Stream[Int] = cons(n, from(n + 1))

  def fibs: Stream[Int] = {
    def go(first: Int, second: Int): Stream[Int] = cons(first, go(second, first + second))

    go(0, 1)
  }

  def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] = f(z) match {
    case Some((a, s)) => cons(a, unfold(s)(f))
    case None => Empty
  }

  def fibsV2: Stream[Int] = unfold(0, 1){
    case (first, second) => Some((first, (second, first + second)))
  }

  def fromV2(n0: Int): Stream[Int] = unfold(n0)(n => Some((n, n + 1)))

  def constantV2[A](a0: A): Stream[A] = unfold(a0)(a => Some((a, a)))

  def onesV2: Stream[Int] = unfold(1)(_ => Some((1, 1)))
}
