package Chapter5

object Tails extends App {
  println(Stream.apply(1, 2, 3).tails.map(_.toList).toList)
  println(Empty.tails.map(_.toList).toList)
}
