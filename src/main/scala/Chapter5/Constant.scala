package Chapter5

object Constant extends App {
  println(Stream.constant(2).take(5).toList)
  println(Stream.constantV2(2).take(5).toList)
}
