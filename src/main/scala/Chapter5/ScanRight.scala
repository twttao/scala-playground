package Chapter5

object ScanRight extends App {
  println(Stream.apply(1, 2, 3).scanRight(0)(_ + _).toList)
}
