package Chapter5


object HeadOption extends App {
  // variadic parameters in Stream.apply will evaluate once
  val stream = Stream.apply({println(2); 1 + 1}, {println(4); 2 + 2}, {println(6); 3 + 3})
  println(stream.headOption)
  println(stream.headOptionV2)
}
