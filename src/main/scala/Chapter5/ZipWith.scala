package Chapter5

object ZipWith extends App {
  val stream1 = Stream.apply(1, 2, 3, 4, 5)
  val stream2 = Stream.apply(1, 1, 1, 1, 1, 1)

  println(stream1.zipWith(stream2)(_ + _).toList)
}
