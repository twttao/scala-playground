package Chapter5

object Ones extends App {
  println(Stream.ones.take(10).toList)
  println(Stream.onesV2.take(10).toList)
  println(Stream.ones.map(_ + 1).exists(_ % 2 == 0))
  println(Stream.onesV2.map(_ + 1).exists(_ % 2 == 0))
}
