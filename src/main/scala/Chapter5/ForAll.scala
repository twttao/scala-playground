package Chapter5

object ForAll extends App {
  val stream = Stream.apply({1 + 1}, {2 + 2}, {3 + 3}, {4 + 4})

  println(Empty.forAll(_ => true))
  println(stream.forAll(_ % 2 == 0))
  println(stream.forAll(_ + 1 == 3))
}
