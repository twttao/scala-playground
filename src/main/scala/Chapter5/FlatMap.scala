package Chapter5

object FlatMap extends App {
  val stream = Stream.apply({1 + 1}, {2 + 2}, {3 + 3}, {4 + 4})

  println(Empty.flatMap(_ => Stream.apply(true, false)).toList)
  println(stream.flatMap(_ => Stream.apply(true, false)).toList)
  println(stream.flatMap(n => Stream.apply(n - 1, n - 2)).toList)
}
