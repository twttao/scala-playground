package Chapter5

object ZipAll extends App {
  val stream1 = Stream.apply(1, 2, 3, 4, 5)
  val stream2 = Stream.apply(1, 1, 1, 1, 1, 1)

  println(stream1.zipAll(stream2).toList)
}
