package Chapter5

object StartsWith extends App {
  val stream1 = Stream.apply(1, 2, 3, 4 ,5)
  val stream2 = Stream.apply(1, 2, 3)
  val stream3 = Stream.apply(4, 5)
  val stream4 = Stream.apply(1, 2, 3, 4, 5, 6, 7)

  println(stream1.startsWith(Empty))  //true
  println(stream1.startsWithV2(Empty))  //true
  println(Empty.startsWith(Empty))  //true
  println(Empty.startsWithV2(Empty))  //true
  println(Empty.startsWith(stream1))  //false
  println(Empty.startsWithV2(stream1))  //false

  println(stream1.startsWith(stream3))  //false
  println(stream1.startsWithV2(stream3))  //false
  println(stream1.startsWith(stream2))  //true
  println(stream1.startsWithV2(stream2))  //true
  println(stream1.startsWith(stream4))  //false
  println(stream1.startsWithV2(stream4))  //false
}
