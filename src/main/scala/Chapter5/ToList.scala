package Chapter5

import Chapter5.Stream._

object ToList extends App {
  val stream = cons({println(1); 1}, cons({println(2); 2}, empty))
  println(stream.toList)
  println(stream.toListSafe)
}
