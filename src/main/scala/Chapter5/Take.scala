package Chapter5

object Take extends App {
  val stream = Stream.apply({println(2); 1 + 1}, {println(4); 2 + 2}, {println(6); 3 + 3}, {println(8); 4 + 4})
  println(stream.take(2).toList)
  println(stream.takeV2(2).toList)
  println(stream.take(4).toList)
  println(stream.takeV2(4).toList)
  println(stream.take(7).toList)
  println(stream.takeV2(7).toList)
  println(Empty.take(7).toList)
  println(Empty.takeV2(7).toList)
}
