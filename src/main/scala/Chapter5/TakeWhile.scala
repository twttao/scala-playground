package Chapter5

object TakeWhile extends App {
  val stream = Stream.apply({1 + 1}, {2 + 2}, {3 + 3}, {4 + 4})

  println(Empty.takeWhile(_ => true).toList)
  println(Empty.takeWhileV2(_ => true).toList)
  println(Empty.takeWhileV3(_ => true).toList)
  println(stream.takeWhile(_ => true).toList)
  println(stream.takeWhileV2(_ => true).toList)
  println(stream.takeWhileV3(_ => true).toList)
  println(stream.takeWhile(n => n == 2 || n == 4).toList)
  println(stream.takeWhileV2(n => n == 2 || n == 4).toList)
  println(stream.takeWhileV3(n => n == 2 || n == 4).toList)
}
