package Chapter5

object Append extends App {
  val stream = Stream.apply({1 + 1}, {2 + 2}, {3 + 3}, {4 + 4})

  println(Empty.append(Empty).toList)
  println(Empty.append(stream).toList)
  println(stream.append(stream).toList)
  println(stream.append(Empty).toList)
}
