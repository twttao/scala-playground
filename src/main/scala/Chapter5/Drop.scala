package Chapter5

object Drop extends App {
  val stream = Stream.apply({1 + 1}, {2 + 2}, {3 + 3}, {4 + 4})

  println(Empty.drop(1).toList)
  println(stream.drop(0).toList)
  println(stream.drop(1).toList)
  println(stream.drop(2).toList)
  println(stream.drop(4).toList)
  println(stream.drop(6).toList)
}
