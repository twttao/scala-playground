package Chapter11

trait Monad[F[_]] extends Functor[F] {
  def unit[A](a: => A): F[A]

  def flatMap[A,B](fa: F[A])(f: A => F[B]): F[B]

  def map[A,B](fa: F[A])(f: A => B): F[B] = flatMap(fa)(a => unit(f(a)))

  def map2[A,B,C](fa: F[A], fb: F[B])(f: (A, B) => C): F[C] =
    flatMap(fa)(a => map(fb)(b => f(a, b)))

  def sequence[A](lma: List[F[A]]): F[List[A]] = lma match {
    case Nil => unit(Nil)
    case ma::_lma => map2(ma, sequence(_lma))(_::_)
  }

  def traverse[A,B](la: List[A])(f: A => F[B]): F[List[B]] = la match {
    case Nil => unit(Nil)
    case a::_as => map2(f(a), traverse(_as)(f))(_::_)
  }

  def replicateM[A](n: Int, ma: F[A]): F[List[A]] = sequence(List.fill(n)(ma))

  def product[A,B](ma: F[A], mb: F[B]): F[(A, B)] = map2(ma, mb)((_, _))

  def filterM[A](ms: List[A])(f: A => F[Boolean]): F[List[A]] = ms match {
    case Nil => unit(Nil)
    case m::_ms => map2(f(m), filterM(_ms)(f))((bool, ms) => if(bool) m::ms else ms)
  }

  def compose[A,B,C](f: A => F[B], g: B => F[C]): A => F[C] =
    a => flatMap(f(a))(g)

  // flatMap by compose
  def flatMapV2[A,B](fa: F[A])(f: A => F[B]): F[B] = compose((_: Unit) => fa, f)()

  def join[A](mma: F[F[A]]): F[A] = flatMap(mma)(ma => ma)

  // flatMap by join & map
  def flatMapV3[A,B](fa: F[A])(f: A => F[B]): F[B] = join(map(fa)(f))
}

class OptionM extends Monad[Option] {
  def unit[A](a: => A): Option[A] = Some(a)

  def flatMap[A,B](oa: Option[A])(f: A => Option[B]): Option[B] = oa flatMap f
}
