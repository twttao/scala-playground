package Chapter11

case class Id[A](value: A) {
  def map[B](f: A => B): Id[B] = Id(f(value))
  def flatMap[B](f: A => Id[B]): Id[B] = f(value)
}

object Exercise11_7 extends App {
  val res = for {
    a <- Id("Hello, ")
    b <- Id("monad!")
  } yield a + b

  println(res)
}
