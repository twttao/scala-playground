package Chapter11

import Chapter6.State

class StateMonads[S] {
  type StateS[A] = State[S, A]

  val monad: Monad[StateS] = new Monad[StateS] {
    def unit[A](a: => A): State[S, A] = State(s => (a, s))
    override def flatMap[A,B](st: State[S, A])(f: A => State[S, B]): State[S, B] = State(s => {
      val (a, s1) = st.run(s)
      f(a).run(s1)
    })
  }
}

object Exercise11_2 {
}
