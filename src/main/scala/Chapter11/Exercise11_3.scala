package Chapter11

// moved to Monad.scala
// class OptionM extends Monad[Option]

object Exercise11_3 extends App {
  val listOfOptions = List.apply(Some(2), Some(3), Some(4))
  println(new OptionM().sequence(listOfOptions))

  val listOfInt = List.apply(2, 3, 4)
  val mapOnlyEven: Int => Option[Int] = i => if (i % 2 == 0) Some(i) else None
  val mapAll: Int => Option[Int] = i => Some(i)
  println(new OptionM().traverse(listOfInt)(mapOnlyEven))
  println(new OptionM().traverse(listOfInt)(mapAll))
}
