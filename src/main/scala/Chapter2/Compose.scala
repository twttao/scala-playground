package Chapter2

object Compose extends App {
  val addOne = (a: Int) => a + 1
  val multiplyTwo = (x: Int) => x * 2
  val addOneThenMultiplyTwo = compose(multiplyTwo, addOne)
  val multiplyTwoThenAddOne = compose(addOne, multiplyTwo)

  println(addOneThenMultiplyTwo(3))
  println(addOneThenMultiplyTwo(5))

  println(multiplyTwoThenAddOne(3))
  println(multiplyTwoThenAddOne(5))

  def compose[A,B,C] (f: B => C, g: A => B): A => C = {
    (a: A) => f(g(a))
  }
}
