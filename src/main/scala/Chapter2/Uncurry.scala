package Chapter2

object Uncurry extends App {
  val add = uncurry((a: Int) => (b: Int) => a + b)
  println(add(3, 7))
  println(add(5, 9))

  def uncurry[A,B,C] (f: A => B => C): (A, B) => C = {
    (a: A, b: B) => f(a)(b)
  }
}
