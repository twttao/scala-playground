package Chapter2

object Currying extends App {
  val plus = curry((x: Int, y: Int) => x + y)
  println(plus(1)(2))
  println(plus(3)(5))

  val multiply = curry((x: Int, y: Int) => x * y)
  println(multiply(4)(3))
  println(multiply(7)(8))

  def curry[A,B,C] (f: (A,B) => C): A => (B => C) = {
    (a: A) => (b: B) => f(a, b)
  }
}
