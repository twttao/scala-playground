package Chapter2

object IsSorted extends App {
  println(isSorted(Array(1, 2, 3, 4, 5), (x: Int, y: Int) => x <= y))
  println(isSorted(Array(1, 3, 2, 4, 5), (x: Int, y: Int) => x <= y))
  println(isSorted(Array(2, 2, 2, 2, 2), (x: Int, y: Int) => x <= y))
  println(isSorted(Array(10), (x: Int, y: Int) => x < y))

  def isSorted[A](as: Array[A], ordered: (A, A) => Boolean): Boolean = {
    def loop(n: Int): Boolean =
      if (n >= as.length) {
        true
      } else if (n > 1 && !ordered(as(n - 1), as(n))) {
        false
      } else {
        loop(n + 1)
      }

    loop(0)
  }
}
