package Chapter2

object Fibonacci extends App{
  println(Array(fib(1), fib(2), fib(3), fib(4), fib(5), fib(6), fib(7)).mkString(","))
  println(Array(fib2(1), fib2(2), fib2(3), fib2(4), fib2(5), fib2(6), fib2(7)).mkString(","))

  def fib(n: Int): Int = {
    if (n == 1) {
      0
    } else if (n == 2) {
      1
    } else {
      fib(n - 1) + fib(n - 2)
    }
  }

  def fib2(n: Int): Int = {
    def fib2Inner(current: Int, prev1: Int, prev2: Int): Int = {
      if (current == n) {
        prev1 + prev2
      } else {
        fib2Inner(current + 1, prev2, prev1 + prev2)
      }
    }

    if (n == 1) {
      0
    } else if (n == 2) {
      1
    } else {
      fib2Inner(3, 0, 1)
    }
  }
}
