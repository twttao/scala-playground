package Chapter4

sealed trait Option[+A] {
  def map[B] (f: A => B): Option[B] = this match {
    case None => None
    case Some(a) => Some(f(a))
  }
  def flatMap[B] (f: A => Option[B]): Option[B] = this match {
    case None => None
    case Some(a) => f(a)
  }
  def getOrElse[B>:A] (default: => B): B = this match {
    case None => default
    case Some(b) => b
  }
  def orElse[B>:A] (ob: => Option[B]): Option[B] = this match {
    case None => ob
    case _ => this
  }
  def filter (f: A => Boolean): Option[A] = this match {
    case Some(a) if f(a) => this
    case _ => None
  }
}

object Option {
  def tryDo[A](a: => A): Option[A] =
    try Some(a)
    catch { case _: Exception => None}

  def mean(xs: Seq[Double]): Option[Double] =
    if (xs.isEmpty) {
      None
    } else {
      Some(xs.sum / xs.length)
    }

  def map2[A,B,C] (oa: Option[A], ob: Option[B])(f: (A,B) => C): Option[C] =
    oa flatMap (a => ob map (b => f(a, b)))

  def map2V2[A,B,C](oa: Option[A], ob: Option[B])(f: (A,B) => C): Option[C] =
    for {
      a <- oa
      b <- ob
    } yield f(a, b)

  def sequence[A] (oas: List[Option[A]]): Option[List[A]] = oas match {
    case Nil => Some(Nil)
    case oa::_oas => map2(oa, sequence(_oas))(_::_)
  }

  def traverse[A,B] (as: List[A])(f: A => Option[B]): Option[List[B]] = as match {
    case Nil => Some(Nil)
    case a::_as => map2(f(a), traverse(_as)(f))(_::_)
  }

  def variance(xs: Seq[Double]): Option[Double] =
    mean(xs) flatMap (m => mean(xs.map(x => math.pow(x - m, 2))))
}

case class Some[+A] (get: A) extends Option[A]
case object None extends Option[Nothing]
