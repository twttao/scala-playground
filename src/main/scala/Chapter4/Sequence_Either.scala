package Chapter4

import Chapter4.Either.sequence

object Sequence_Either extends App {
  println(sequence(List(Right(1), Left(new Exception()), Right(3))))
  println(sequence(List(Right(1), Right(4), Right(3))))
  println(sequence(List.empty))
}
