package Chapter4

import Chapter4.Option.sequence

object Sequence extends App {
  println(sequence(List(Some(1), None, Some(2))))
  println(sequence(List(Some(1), Some(4), Some(2))))
  println(sequence(List.empty))
}
