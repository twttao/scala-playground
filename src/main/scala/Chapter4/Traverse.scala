package Chapter4

import Chapter4.Option.tryDo
import Chapter4.Option.traverse

object Traverse extends App {
  println(traverse(List("2","me","3"))(s => tryDo(s.toInt)))
  println(traverse(List("2","4","3"))(s => tryDo(s.toInt)))
  println(traverse(Nil: List[String])(s => tryDo(s.toInt)))
}
