package Chapter4

import Chapter4.Option.map2
import Chapter4.Option.map2V2

object Map2 extends App {
  println(map2(Some(2), None: Option[Int])(_ + _))
  println(map2V2(Some(2), None: Option[Int])(_ + _))
  println(map2(None: Option[Int], Some(3))(_ + _))
  println(map2V2(None: Option[Int], Some(3))(_ + _))
  println(map2(None: Option[Int], None: Option[Int])(_ + _))
  println(map2V2(None: Option[Int], None: Option[Int])(_ + _))
  println(map2(Some(2), Some(3))(_ + _))
  println(map2V2(Some(2), Some(3))(_ + _))
}
