package Chapter4

sealed trait Either[+E, +A] {
  def map[B] (f: A => B): Either[E, B] = this match {
    case Right(a) => Right(f(a))
    case Left(e) => Left(e)
  }

  def flatMap[EE>:E, B] (f: A => Either[EE, B]): Either[EE, B] = this match {
    case Right(a) => f(a)
    case Left(e) => Left(e)
  }

  def orElse[EE>:E, B>:A] (b: => Either[EE, B]): Either[EE, B] = this match {
    case Left(_) => b
    case _ => this
  }

  def map2[EE>:E, B, C] (b: Either[EE, B])(f: (A, B) => C): Either[EE, C] = {
     this.flatMap(a => b.map(b => f(a, b)))
  }
}

object Either {
  def mean (xs: IndexedSeq[Double]): Either[String, Double] =
    if (xs.isEmpty) {
      Left("mean of empty list")
    } else {
      Right(xs.sum / xs.length)
    }

  def safeDiv (x: Int, y: Int): Either[Exception, Int] =
    try Right(x / y)
    catch { case e: Exception => Left(e) }

  def tryDo[A](a: => A): Either[Exception, A] =
    try Right(a)
    catch { case e: Exception => Left(e) }

  def sequence[E, A] (es: List[Either[E, A]]): Either[E, List[A]] =
    traverse(es)(x => x)

  def traverse[E, A, B] (as: List[A])(f: A => Either[E, B]): Either[E, List[B]] = as match {
    case Nil => Right(Nil)
    case e::_es => f(e).map2(traverse(_es)(f))(_::_)
  }
}

case class Left[+E] (value: E) extends Either[E, Nothing]
case class Right[+A] (value: A) extends Either[Nothing, A]

