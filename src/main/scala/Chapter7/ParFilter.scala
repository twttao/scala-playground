package Chapter7

import java.util.concurrent.Executors
import Chapter7.Par._

object ParFilter extends App {
  val executor = Executors.newCachedThreadPool()
  val list = List(1, 2, 3, 4, 5, 6)
  val parFilterOdd = parFilter(list)(_ % 2 == 0)

  println(run(executor)(parFilterOdd).get)
  executor.shutdown()
}
