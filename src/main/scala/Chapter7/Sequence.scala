package Chapter7

import java.util.concurrent.Executors
import Chapter7.Par._

object Sequence extends App {
  val executor = Executors.newCachedThreadPool()
  val parOfList = sequence(List(unit(1), unit(2), unit(3)))

  println(run(executor)(parOfList).get)

  executor.shutdown()
}
