package Chapter7

import java.util.concurrent.Executors
import Chapter7.Par._

object ParMap extends App {
  val executor = Executors.newCachedThreadPool()
  val list = List(1, 2, 3, 4, 5, 6)
  val parDoubleList = parMap(list)(_ * 2)

  println(run(executor)(parDoubleList).get)
  executor.shutdown()
}
